# GoChain

![GoChain Logo](./assets/gochain.png)

**GoChain** is a web server written in Go that provides functionality for taking screenshots of websites with caching, generating QR codes, and acting as a web proxy.

---

## API
```
GET | /
```
Takes a screenshot of a website with a resolution of 1366x768. It only supports one URL parameter: url.

Example URL: `http://gochain.local/?url=https://naomi.win`

URL Parameters:
- url: string - The address of the website to take a screenshot of.

--- 
```
GET | /qr.png
```
Generates a black and white QR code image. It only supports one URL parameter: text.

Example URL: `http://gochain.local/qr.png?text=Join us > [ https://naomi.win ]!!`

URL Parameters:
- text: string - The text to be encoded in the QR code.

---

```
ANY | /proxy/:url
```
Proxies the website specified by :url and returns its content.

Example URL: `http://gochain.local/proxy/https://github.com`

URL Parameters: **none**

---

```
POST | /shot
```
A more advanced way of taking screenshots of websites. Supports custom screen resolutions.

Example Code (python requests):
```py
import requests

requests.post('http://gochain.local/shot', json={'url': 'https://naomi.win', 'screen': [2560, 1440]})
```

URL Parameters: **none**

JSON Parameters:
- url: string - The address of the website to take a screenshot of.
- screen: [2]int64 - The screen resolution (first argument is width, second argument is height: [width, height]).
- delay: int - Custom delay before taking a screenshot. By default, the value of the default delay is set.

---

## Environment Variables
The following environment variables can be configured:
1. `PORT`: Sets the port on which the web server listens.
2. `COOLDOWN`: Sets the delay before taking another screenshot. This acts as a rate limiter.
3. `WAIT_BEFORE_SCREENSHOT`: Specifies the number of seconds Chromium waits after navigating to a website before taking a screenshot. This allows the website to fully load and prevents capturing a blank screen.
4. `CACHE_CLEANUP_INTERVAL`: Sets the interval in seconds at which the screenshot cache folder is cleaned up.
5. `PROXY`: Specifies the address of the proxy server for Chromium.
