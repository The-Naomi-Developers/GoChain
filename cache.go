package main

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"
)

// Cache screenshot
func cache(buf []byte, image string) error {
	urlEnc := base64.URLEncoding.EncodeToString([]byte(image))
	if err := ioutil.WriteFile(cacheDir+urlEnc+".png", buf, 0o644); err != nil {
		return err
	}
	fmt.Printf("[INFO] Successfully cached screenshot for image '%s'\n", image)
	return nil
}

// Get cached screenshot
func readFromCache(image string, res *[]byte) error {
	var err error
	urlEnc := base64.URLEncoding.EncodeToString([]byte(image))
	*res, err = ioutil.ReadFile(cacheDir + urlEnc + ".png")
	return err
}

func deleteFilesPeriodically() {
	for {
		deleteFiles(cacheDir)
		time.Sleep(time.Second * time.Duration(cacheCleanUpInterval))
	}
}

func deleteFiles(dir string) {
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() {
			err := os.Remove(path)
			if err != nil {
				fmt.Println("[ERROR] Unable to delete cache file '"+info.Name()+"':", err)
			}
		}
		return nil
	})

	if err != nil {
		fmt.Println("[ERROR] Cannot iterate over cache directory:", err)
	}
}
