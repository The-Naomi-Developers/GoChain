FROM golang:latest as builder
WORKDIR /go/src/naomi/gochain
COPY ./ ./
RUN go mod tidy
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o gochain

FROM alpine:latest as runtime
WORKDIR /app
RUN apk update && apk add chromium
COPY --from=builder /go/src/naomi/gochain/gochain ./
RUN chmod +x /app/gochain
CMD ["/app/gochain"]
